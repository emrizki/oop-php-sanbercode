<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$this_animal = new Animal("Shaun");

echo "Nama Binatang: ".$this_animal->name. "<br>";
echo "Jumlah Kaki: ".$this_animal->legs."<br>";
echo "Berdarah digin: ".$this_animal->cold_blooded."<br><br>";


$this_frog = new Frog('Frog');

echo "Nama Binatang: ".$this_frog->name."<br>";
echo "Jumlah Kaki: ".$this_frog->legs."<br>";
echo "Berdarah digin: ".$this_frog->cold_blooded."<br>";
echo "Suara: ".$this_frog->jump()."<br><br>";

$this_ape = new Ape('Ape');

echo "Nama Binatang: ".$this_ape->name."<br>";
echo "Jumlah Kaki: ".$this_ape->legs."<br>";
echo "Berdarah digin: ".$this_ape->cold_blooded."<br>";
echo "Suara: ".$this_ape->yell()."<br><br>";


?>