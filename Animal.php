<?php

class Animal {
  public $name;
  public $legs = 2;
  public $cold_blooded = "False";

  public function __construct($input)
  {
    $this->name = $input;
  }
}

?>